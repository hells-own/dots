do_configure ()
{
    ./Util/preconfig
    ./configure --prefix=$pkg_dir
}

do_install ()
{
    make install || :
    rmdir $pkg_dir/share/zsh/site-functions
    ln -sT /usr/share/zsh/site-functions $pkg_dir/share/zsh/site-functions
}
