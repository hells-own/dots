do_configure ()
{
    ./autogen.sh
    ./configure --without-x --without-all --prefix=$pkg_dir
}

do_install ()
{
    make install
    rm -f $pkg_dir/bin/etags $pkg_dir/bin/ctags $pkg_dir/bin/ebrowse
}
