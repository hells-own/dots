pkg_url=https://github.com/solvespace/solvespace.git
pkg_builddir=build

do_install_deps ()
{
    case $pkg_type in
        xbps)
            sudo xbps-install cmake zlib-devel libpng-devel freetype-devel \
                 json-c-devel fontconfig-devel gtkmm-devel \
                 pangomm-devel glu-devel
            ;;
    esac
}

do_configure ()
{
    mkdir -p $pkg_builddir
    cmake -S . -B build \
          -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_INSTALL_PREFIX="$pkg_dir"
}

do_install ()
{
    make -C $pkg_builddir install
}
