do_build ()
{
	make clean && make ENABLE_DEBUG=0
}

do_configure ()
{
    :
}

do_install ()
{
    make install DESTDIR=$pkg_dir PREFIX=
}
