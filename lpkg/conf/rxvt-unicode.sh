do_configure () {
    ./configure --disable-perl --disable-frills --disable-mousewheel  --prefix=$pkg_dir
}

do_build () {
    make clean && make -j
}

do_install () {
    make install
}
