do_configure() {
	:
}

do_build() {
	make -j all SYSTEMD=0 PREFIX=$HOME/.local
}

do_install() {
	make install PREFIX=$pkg_dir/
}
