/* See LICENSE file for copyright and license details. */

static const unsigned int snap = 32;       /* snap pixel */
static const int showbar       = 1;        /* 0 means no bar */
static const int topbar        = 0;        /* 0 means bottom bar */
static const int pushnewclient = 1;        /* 0 means pop new client to top */
static const int anchw         = AnchRight;
static const int anchh         = AnchTop;

static const char col_gray0[]  = "#111111";
static const char col_gray1[]  = "#222222";
static const char col_gray2[]  = "#444444";
static const char col_gray3[]  = "#bbbbbb";
static const char col_gray4[]  = "#eeeeee";
static const char col_cyan[]   = "#005577";
static const char col_amber[]  = "#553311";

#include "local_config.h"
#if ENABLE_WARP_PORTAL
#ifndef warp_cfg
static const WarpPortal warp_cfg[] = {{{0}}};
#endif
#endif

#include "clr_config.h"

#define GAP_MAX 32
#define MIN_COLS 90
#define MAX_COLS 110

const char *test_plt[] = {color0, color1, color2, color3, color4, color5, color6};
const unsigned int test_plt_sel[] = {0, 1, 2, 3, 4, 5, 6};
const TileDef test_stripes = TILE_DEF_STRF(test_plt, test_plt_sel, 10);

static const ClrDef colors[] = {
    CLR_DEF_PLAIN (col_gray3),
    CLR_DEF_PLAIN (col_gray1),
    CLR_DEF_TILE  (&test_stripes),

    CLR_DEF_PLAIN (color0),
    CLR_DEF_PLAIN (color1),
    CLR_DEF_PLAIN (color2),
    CLR_DEF_PLAIN (color3),
    CLR_DEF_PLAIN (color4),
    CLR_DEF_PLAIN (color5),
    CLR_DEF_PLAIN (color6),
};

static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       =   "monospace:size=10";
static const unsigned int borderpx  = 2;        /* border pixel of windows */

/* tagging */
static const char *const tags[] = {
	"1",
	"2",
	"4",
	"8",
	"I",
	"Y",
	"P",
};

static const Rule rules[] = {
    /*class     instance  title       tags   flags        monitor  */
	{"",             "",  "popup",       0,  PopupFlags,   -1  },
	{"",             "",  "zsh-launch",  0,  LaunchFlags,  -1  },
	{"qutebrowser",  "",  "",            0,  1<<FlagWide,     -1  },
	{"steam",        "",  "",            0,  1<<FlagWide,     -1  },
	{"Xephyr",       "",  "",            0,  1<<FlagFloat,    -1  },
	{"",			 "",  "Joplin",		 0,  1<<FlagWide,	   -1  },
};

/* layout(s) */
static const float mfact     = 0.35;   /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;      /* number of clients in master area */
static const int resizehints = 1;      /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1;   /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]",   tile_auto },
    { "",     tile },
    { "",     monocle},
};

/* component of dmenucmd, manipulated in spawn() */
static char dmenumon[2] = "0";
static const char *dmenucmd[] = {
    "urxvtc", "-title", "zsh-launch", "-e", "zsh-launch",
    NULL };
static const char *popupcmd[] = {
    "urxvtc", "-title", "popup",
    NULL };
static const char *termcmd[] = {
    "urxvtc",
    NULL };
static const char *volupcmd[] = {
    "pactl", "set-sink-volume", "@DEFAULT_SINK@", "+5%",
    NULL };
static const char *voldowncmd[] = {
    "pactl", "set-sink-volume", "@DEFAULT_SINK@", "-5%",
    NULL };
static const char *volmutecmd[] = {
    "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle",
    NULL };
static const char *blupcmd [] = {
    "light", "-T", "1.600",
    NULL };
static const char *bldowncmd [] = {
    "light", "-T", "0.625",
    NULL };

/* key definitions */
#define MOD Mod1Mask
#define MOD_C MOD | ControlMask
#define MOD_S MOD | ShiftMask
#define MOD_CS MOD | ShiftMask | ControlMask

#define _TAGKEYS(MODEX, KEY, TAG)                       \
    { MOD    | MODEX, KEY, toggleview, {.ui = TAG } },  \
    { MOD_S  | MODEX, KEY, toggletag,  {.ui = TAG } }   \

#define TAGKEYS(KEY, TAG) _TAGKEYS(0,        KEY, TAG)
#define TAGKSUP(KEY, TAG) _TAGKEYS(Mod4Mask, KEY, TAG)

static const Key keys[] = {
    { MOD,    XK_b,            togglebar,  {  0                   } },
    { MOD_S,  XK_c,            killclient, {  0                   } },
    { MOD,    XK_d,            spawn,      { .v = dmenucmd        } },
    { MOD,    XK_f,            flagclient, { .i = -(1 << FlagFloat) } },
    { MOD_S,  XK_f,            setclientfullscreen, { .i = -1 } },
    { MOD,    XK_k,            spawn,      { .v = termcmd         } },
    { MOD_S,  XK_k,            spawn,      { .v = popupcmd        } },
    { MOD,    XK_j,            setlayout,  {  0                   } },
    { MOD,    XK_m,            setmfact,   { .f = +0.15           } },
    { MOD_S,  XK_m,            setmfact,   { .f = -0.15           } },

    { MOD,    XK_n,            focusstack, { .ui = 1              } },
    { MOD_S,  XK_n,            focusstack, { .ui = 3              } },

    { MOD_S,  XK_q,            quit,       {  0                   } },
    { MOD_CS, XK_q,            quit,       {  1                   } },
    { MOD,    XK_r,            iconify,    {  0                   } },
    { MOD_S , XK_r,            reveal,     {  0                   } },

    { MOD,    XK_t,            focusstack, { .ui = 0             } },
    { MOD_S,  XK_t,            focusstack, { .ui = 2             } },

    { MOD,    XK_v,            view,       { .i = -1              } },
    { MOD_S,  XK_v,            tag,        { .i = -1              } },
    { MOD,    XK_w,            flagclient, { .i = -(1 << FlagWide)  } },
    { MOD,    XK_z,            tagall,     { .ui = 0              } },
    { MOD,    XK_Return,       zoom,       { .ui = 0              } },
    { MOD_S,  XK_Return,       zoom,       { .ui = 1              } },
    { MOD,    XK_comma,        focusmon,   { .i = -1              } },
    { MOD,    XK_period,       focusmon,   { .i = +1              } },
    { MOD_S,  XK_comma,        tagmon,     { .i = -1              } },
    { MOD_S,  XK_period,       tagmon,     { .i = +1              } },
    { MOD,    XK_bracketright, spawn,      { .v = volupcmd        } },
    { MOD,    XK_bracketleft,  spawn,      { .v = voldowncmd      } },
    { MOD,    XK_backslash,    spawn,      { .v = volmutecmd      } },
    { MOD_S,  XK_bracketleft,  spawn,      { .v = bldowncmd       } },
    { MOD_S,  XK_bracketright, spawn,      { .v = blupcmd         } },

    TAGKEYS(  XK_0,   0),

    TAGKEYS(  XK_1,   1),
    TAGKEYS(  XK_2,   2),
    TAGKEYS(  XK_3,   3),
    TAGKEYS(  XK_4,   4),
    TAGKEYS(  XK_5,   5),
    TAGKEYS(  XK_6,   6),
    TAGKEYS(  XK_7,   7),
    TAGKEYS(  XK_8,   8),
    TAGKEYS(  XK_9,   9),

    TAGKSUP(  XK_a,  10),
    TAGKSUP(  XK_b,  11),
    TAGKSUP(  XK_c,  12),
    TAGKSUP(  XK_d,  13),
    TAGKSUP(  XK_e,  14),
    TAGKSUP(  XK_f,  15),

    TAGKEYS(  XK_i,  16),
    TAGKEYS(  XK_y,  32),
    TAGKEYS(  XK_p,  64)

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkClientWin,         MOD,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MOD,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MOD,         Button3,        resizemouse,    {0} },
};
