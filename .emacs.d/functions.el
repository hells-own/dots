(require 'subr-x)

(defun xterm-print-buffer-file-name ()
  (when-let ((name (buffer-file-name)))
    (send-string-to-terminal
     (format "\e]2;emacs %s@%s %s\a"
             user-name-cache
             system-name-cache
             name))))

;; commands

(defun _evil-scroll-half-page-down ()
  (interactive)
  (evil-scroll-down 0))

(defun _evil-scroll-half-page-up ()
  (interactive)
  (evil-scroll-up 0))

(defun open-term (arg)
  (interactive "p")
  (let ((term-name (format "*term-%d*" arg))
        (return-buffer (current-buffer)))
    (unless (string= term-name (buffer-name return-buffer))
      (setq secondary-window-return (get-buffer-window return-buffer))
      (if-let ((cur-buffer (get-buffer term-name)))
          (switch-to-buffer-other-window cur-buffer)
        (let ((new-term (shell)))
          (switch-to-buffer new-term)
          (rename-buffer term-name))))))

(defun reload-config ()
  (interactive)
  (load-file "~/.emacs"))

(defun _urxvt-clip (start end)
  (interactive "r")
  (let ((str (buffer-substring-no-properties start end)))
    (send-string-to-terminal
     (format "\e]901;%s\a" str))
    (minibuffer-message str)))

(defun indent-region-column (start end)
  (interactive "r")
  (evil-ex "'<,'>!column -t")
  (indent-region start end))

(defun window-zoom ()
  (interactive)
  (let ((win-focus (get-buffer-window nil (frame-focus)))
		(win-list (window-list (frame-focus))))
	(dolist (win win-list)
	  (unless (eq win win-focus)
		(delete-window win)))))


;; project commands

(defun find-upward (file dir)
  "search for file in the current directory or a parent"
  (if (file-exists-p (concat dir file))
      (expand-file-name dir)
    (if (string= (expand-file-name dir) "/")
        nil
      (find-upward file (concat dir "../")))))

(defun get-project-cmdline (command &optional args no-error)
  (when project-dir
    (format "project-shell %s %s %s %s %s" project-dir command
            (buffer-file-name)
            (string-join args " ")
			(if no-error "2>/dev/null" "2>&1"))))

(defun project-cmd-async (command &optional args no-error)
  "execute a project command asyncronously in the compilation buffer"
  (interactive)
  (when-let ((project-shell (get-project-cmdline command args no-error)))
    (compile project-shell)))

(defun project-cmd-sync (cmd &optional use-minibuffer args no-error)
  "execute a project command synchronously. if use-minibuffer is specified,
display the result, otherwise return it"
  (let* ((shell-command (get-project-cmdline cmd args no-error))
         (result (shell-command-to-string shell-command)))
    (when use-minibuffer (minibuffer-message result))
    result))

(defun init-buffer-project ()
  (setq-local project-dir (find-upward "_project.sh" "./"))
  (setq-local project-dir-name
			  (if project-dir (file-name-base (directory-file-name project-dir))))
  (setq-local project-root
			  (if project-dir
				  (string-trim (project-cmd-sync "get-root"))
				nil))
  (when (and project-root
			 (file-exists-p (string-join (list project-root "TAGS") "/")))
    (visit-tags-table project-root)
    (setq-local ggtags-project-root project-root)))

(defun project-translate ()
  (interactive)
  (project-cmd-async "translate"))

(defun project-build ()
  (interactive)
  (project-cmd-async "build"))

(defun project-launch ()
  (interactive)
  (project-cmd-async "launch"))

(defun project-rebuild ()
  (interactive)
  (project-cmd-async "rebuild"))

(defun project-rebuild-tags ()
  (interactive)
  (project-cmd-async "rebuild-tags")
  (find-file-tags))

(defun project-debug ()
  (interactive)
  (let ((cmd (project-cmd-sync "debug" nil nil t)))
    (gud-gdb (string-trim cmd))))
