if [ $SHLVL -gt 1 ] && [ -z "$LZSH_EXEC" ] && [ -x ~/lpkg/bin/zsh ]
then
    export LZSH_EXEC=y
    exec ~/lpkg/bin/zsh
fi

if [ -n "$LZSH_EXEC" ]
then
    EPROMPT="!"
    export LZSH_NOEXEC=
fi

[[ $ZSH_DEPTH -ge 1 ]] && EPROMPT="$ZSH_DEPTH$EPROMPT"
export ZSH_DEPTH=$((ZSH_DEPTH + 1))

[ -n "$EPROMPT" ] && PROMPT="[$EPROMPT]$OPROMPT"

# load completion
autoload -U compinit; compinit

# default
LS="ls --color=auto"

export FZF_DEFAULT_OPTS="\
-i -x -e \
--algo=v1 --tiebreak=chunk \
--border=none --padding=0 --margin=0 \
--no-scrollbar --separator=' ' \
--color=\
gutter:0\
,bg+:8\
,fg+:7:bold\
,hl+:1:bold\
,hl:1:bold\
,info:6\
"

[ -x ~/.zlocal ] && source ~/.zlocal

## aliases
alias sudo="sudo --preserve-env=TERMINFO"
if which vim >&/dev/null; then
	export EDITOR=vim
else
	if which vi >&/dev/null; then
		export EDITOR=vi
	fi
fi
if [ -S "$XDG_RUNTIME_DIR"/emacs/server ]; then
	export ALTERNATE_EDITOR=$EDITOR
	EC='emacsclient -tc'
	export EDITOR="$EC"
	alias ec="$EC"
fi
alias emacs="emacs -nw"
alias st-prog="st-flash --format ihex write"
alias view="vim -RMX"

alias l="$LS"
alias ls="$LS"
alias la="$LS -a"
alias ll="$LS -l"
alias lh="$LS -lh"
alias lah="$LS -lha"
alias estart="enlightenment_start"

alias rs='rsync -rlpt -v -Ph'
alias gd='git diff --no-index --color=always --'

umv() {
    [ "$#" -eq "2" ] ||return 1
    mv "$1" "$2"
    mv "$1".meta "$2".meta
}

## zshzle
bindkey -v
bindkey -a "h" vi-backward-char
bindkey -a "t" down-line-or-history
bindkey -a "n" up-line-or-history
bindkey -a "s" vi-forward-char
bindkey -a "l" vi-repeat-search
bindkey -a "L" vi-rev-repeat-search
bindkey -a "k" vi-find-next-char-skip
bindkey -a "K" vi-find-prev-char-skip
bindkey -a " s" push-input
bindkey -ar " "
bindkey -a " r" get-line
bindkey -M viopp "h" vi-backward-char
bindkey -M viopp "t" down-line-or-history
bindkey -M viopp "n" up-line-or-history
bindkey -M viopp "s" vi-forward-char
bindkey -M viopp "l" vi-repeat-search
bindkey -M viopp "L" vi-rev-repeat-search
bindkey -M viopp "k" vi-find-next-char-skip
bindkey -M viopp "K" vi-find-prev-char-skip
# treat reflexive :w as accept-line
zle -A accept-line w
zle -A accept-line x

## zshopt
# don't add duplicates to history
setopt HIST_IGNORE_ALL_DUPS

setopt ALWAYS_TO_END
setopt BAD_PATTERN
setopt BRACE_CCL
setopt COMPLETE_IN_WORD
setopt GLOB_COMPLETE
setopt GLOB_STAR_SHORT
setopt LIST_PACKED
setopt MARK_DIRS
setopt NOMATCH
unsetopt AUTO_MENU
unsetopt BEEP

# dmenu replacement
if builtin test -n "$ZSH_LAUNCHER" ; then
    PS1='
  %% '
    CMD=
    preexec () {
        CMD=$3
    }
    precmd () {
        RET=$?
        builtin test -n "$CMD" || return
        builtin test 0 = "$RET" && exit 0
        builtin printf "\n (%d) %s\n" "$?" "$CMD"
        # set urxvt to map on bell
        builtin printf '\a'
    }
else
    # xterm title
    if builtin test -n "$DISPLAY" ; then
        precmd () {
            builtin print -Pnf "\e]0;%s\a" "zsh %n@%m %d"
        }
    fi
fi
