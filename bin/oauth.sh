################################################################################
#
# OAUTH_APPLICATON_ROOT
#   Tokens will be stored in $OAUTH_APPLICATION_ROOT/.oauth
#
# OAUTH_CLIENT_SCOPES
#   List of scopes to request separated by '%20'
#
# OAUTH_CLIENT_ID
#   Generated client ID
#
# OAUTH_CLIENT_SECRET
#   Generated client pre-shared secret
#
# OAUTH_AUTHORIZE_ENDPOINT
#   url to request initial authorization
#
# OAUTH_TOKEN_REQ_ENDPOINT
#   url to get or refresh token
#
################################################################################


: \
    && [ -d "$OAUTH_APPLICATION_ROOT" ] \
    && [ -n "$OAUTH_CLIENT_ID" ] \
    && [ -n "$OAUTH_CLIENT_SECRET" ] \
    && [ -n "$OAUTH_CLIENT_SCOPES" ] \
    && [ -n "$OAUTH_AUTHORIZE_ENDPOINT" ] \
    && [ -n "$OAUTH_TOKEN_REQ_ENDPOINT" ] \
    || exit 1

OAUTH_STORAGE="$OAUTH_APPLICATION_ROOT"/.oauth
[ -d "$OAUTH_STORAGE" ] || mkdir -p $OAUTH_STORAGE
chmod 700 $OAUTH_STORAGE || exit 4
OAUTH_CODE_FILE="$OAUTH_STORAGE"/code
OAUTH_TOKEN_FILE="$OAUTH_STORAGE"/token

OAUTH_CALLBACK_PORT=4200
OAUTH_REDIRECT_URI="http://localhost:$OAUTH_CALLBACK_PORT"

OAUTH_RANDOM_SOURCE=/dev/random
test -r /dev/urandom && OAUTH_RANDOM_SOURCE=/dev/urandom
OAUTH_RANDOM_STATE=`tr -dc '[:alnum:]' <$OAUTH_RANDOM_SOURCE | head -c 32`

OAUTH_CLIENT_AUTH=$(echo -n "$OAUTH_CLIENT_ID:$OAUTH_CLIENT_SECRET" | base64 -w0 -)

OAUTH_AUTH_REQ="\
$OAUTH_AUTHORIZE_ENDPOINT\
?response_type=code\
&client_id=$OAUTH_CLIENT_ID\
&redirect_uri=$OAUTH_REDIRECT_URI\
&state=$OAUTH_RANDOM_STATE\
&scope=$OAUTH_CLIENT_SCOPES\
"

OAUTH_TOKEN_FORMAT='@text "
OAUTH_TOKEN=\(.access_token) ;
OAUTH_TOKEN_EXPIRES=\(.expires_in+now | trunc) ;
OAUTH_REFRESH_TOKEN=\(.refresh_token) ;
"'

if ! which netcat >/dev/null ; then
    which nc >/dev/null || exit 3
    alias netcat=nc
fi


OAUTH_GET_TOKEN_CODE(){
    echo "Authorize this request:" > /dev/stderr
    echo $OAUTH_AUTH_REQ > /dev/stderr

    echo "trying to copy with xclip..." > /dev/stderr
    if which xclip >/dev/null && echo "$OAUTH_AUTH_REQ" | xclip -sel clip ; then
        echo coppied to clipboard >/dev/stderr
    else
        echo xclip is not installed or copy failed >/dev/stderr
    fi

    echo "close the tab to continue" > /dev/stderr
    OAUTH_RESP=$(netcat -l -p $OAUTH_CALLBACK_PORT 2>&1 \
                     | tr '?& ' '\n' \
                     | grep -E '^(code|state)')
    echo "$OAUTH_RESP" | grep "^state=$OAUTH_RANDOM_STATE" >/dev/null || exit 5
    echo "$OAUTH_RESP" | grep "^code" | cut -d '=' -f 2
}

OAUTH_REPLACE_VALUE(){
    test xnull = x"$3" && return
    sed -i "$1" -e "/^$2=/d"
    echo "$2=$3" >> "$1"
}

OAUTH_GET_TOKEN_FROM_CODE(){
    OAUTH_GRANT_TYPE=$1
    read OAUTH_CODE
    case $OAUTH_GRANT_TYPE in
        authorization_code ) OAUTH_CODE_TYPE=code ;;
        refresh_token      ) OAUTH_CODE_TYPE=refresh_token ;;
        * ) echo "unknown code type" >/dev/stderr ; return 1 ;;
    esac
    eval $(curl $CURLOPTS \
                --data-urlencode "grant_type=$OAUTH_GRANT_TYPE" \
                --data-urlencode "$OAUTH_CODE_TYPE=$OAUTH_CODE" \
                --data-urlencode "redirect_uri=$OAUTH_REDIRECT_URI" \
                --header "Authorization: Basic $OAUTH_CLIENT_AUTH" \
                "$OAUTH_TOKEN_REQ_ENDPOINT" \
               | jq -r "$OAUTH_TOKEN_FORMAT"  \
               | tr -s ' ' '\n')
    OAUTH_REPLACE_VALUE $OAUTH_TOKEN_FILE OAUTH_TOKEN $OAUTH_TOKEN
    OAUTH_REPLACE_VALUE $OAUTH_TOKEN_FILE OAUTH_TOKEN_EXPIRES $OAUTH_TOKEN_EXPIRES
    OAUTH_REPLACE_VALUE $OAUTH_TOKEN_FILE OAUTH_REFRESH_TOKEN $OAUTH_REFRESH_TOKEN
}

OAUTH_GET_OR_REFRESH_TOKEN(){
    if [ -r $OAUTH_TOKEN_FILE ] ; then
        . $OAUTH_TOKEN_FILE
        # current token
        [ "$OAUTH_TOKEN_EXPIRES" -gt "$(date -u +%s)" ] && return 0
    fi
    # refresh
    if ! [ -z "$OAUTH_REFRESH_TOKEN" ] ; then
        echo "$OAUTH_REFRESH_TOKEN" | OAUTH_GET_TOKEN_FROM_CODE refresh_token
        return $?
    fi
    # get code
    OAUTH_GET_TOKEN_CODE | OAUTH_GET_TOKEN_FROM_CODE authorization_code
    return $?
}

OAUTH_GET_OR_REFRESH_TOKEN
. $OAUTH_TOKEN_FILE
export OAUTH_TOKEN
