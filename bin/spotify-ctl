#!/bin/sh

# depends:
# curl netcat jq fzf

CACHE=${XDG_CACHE_DIR:-$HOME/.cache}/spotify
mkdir -p $CACHE
API=https://api.spotify.com/v1

################################################################################
# AUTH
################################################################################

. $CACHE/spotify.env
OAUTH_APPLICATION_ROOT="$CACHE"
OAUTH_CLIENT_SCOPES="\
user-read-playback-state%20\
user-modify-playback-state%20\
user-read-currently-playing%20\
user-library-read"
OAUTH_AUTHORIZE_ENDPOINT='https://accounts.spotify.com/authorize'
OAUTH_TOKEN_REQ_ENDPOINT='https://accounts.spotify.com/api/token'

. oauth.sh

################################################################################

test -r $CACHE/device_sel && . $CACHE/device_sel

#FZF='fzf --no-sort --with-nth=2..'
FZF='fzf --no-sort'

REQUEST ()
{
    TYPE=$1
    WHAT=$2
    DATA=$3
    [ -z "$DATA" ] && DATA="{}"
    if ! [ $TYPE = GET ] ; then
        DATAFL=--data
    fi
    URL=$API/$WHAT
    curl $CURLOPTS -L -s --request $TYPE \
         --url $URL \
         --header "Authorization: Bearer $OAUTH_TOKEN" \
         --header "Content-Type: application/json" \
         $DATAFL "$DATA"
}

request_stop ()
{
    REQUEST PUT me/player/pause
}

request_resume ()
{
    test -n "$ACTIVE_DEVICE" && DATA="?device_id=$ACTIVE_DEVICE"
    test -n "$1" && CONTEXT_URI="{ \"context_uri\": \"$1\" }"
    echo "$CONTEXT_URI"
    REQUEST PUT "me/player/play$DATA" "$CONTEXT_URI"
}

request_forward ()
{
    REQUEST POST me/player/next
}

request_back ()
{
    REQUEST POST me/player/previous
}

request_status ()
{
    RESP="$(REQUEST GET me/player)"
    PLAYING=$(echo $RESP | jq -r '.is_playing')
    case $PLAYING in
        true) echo $RESP | jq -r '.item.name' ;;
        *) echo "not playing" ;;
    esac
}

request_device ()
{
    REQUEST GET me/player/devices  | {
        jq -r ".devices | .[] | .id, .name, .is_active"
        echo EOF
    } | while :
    do
        read id; read name; read active
        test "$id" = EOF && break
        case $active in
            true) active='*' ;;
            *) active=' ' ;;
        esac
        echo "$id $active $name"
    done | $FZF | tee /dev/stderr | cut -d' ' -f1 | {
        read selected
        REQUEST PUT me/player "{\"device_ids\":[\"$selected\"]}"
        echo "ACTIVE_DEVICE=$selected" > $CACHE/device_sel
    }
}

fetch_album_list ()
{
    lim=50
    i=0
    while REQUEST GET "me/albums?limit=$lim&offset=$i" \
            | jq -r '.items
| if . == [] then ""|halt_error else .[] end
| (.album | .id, .artists[0].name, .name, .total_tracks), .added_at, (
    .album.tracks.items | .[] | .id, .name
)'
    do
        i=$((lim + i))
    done
    echo EOF
}

set_album_added ()
{
	file=$1
	when=$2
	head -n1 $file | {
		read _when rest
		echo $when $rest
		sed '1d' $file
	} >$file.new
	mv $file.new $file
}

_request_refresh_album ()
{
	MONTH=$((3600 * 24 * 28))
	NOW=$(date -u +%s)
    fetch_album_list | while :
    do
        read album_id; read artist_name; read album_name; read total_tracks
		read added_at
        test "$album_id" = "EOF" && break
        filename=$(echo "${artist_name}_${album_name}_${album_id}" \
					   | tr -dc '[:alnum:]_')
        if [ -f "$CACHE/albums.bak/$filename" ]
        then
            mv $CACHE/albums.bak/$filename $CACHE/albums/$filename
            for i in `seq $total_tracks`
            do
                read _track_id ; read _track_name
            done
        else
            echo "$album_id" "$artist_name" "$album_name"
            {
				echo "0 0 $NOW $MONTH"
                echo "$album_id $artist_name $album_name"
                for i in `seq $total_tracks`
                do
                    read track_id ; read track_name
                    echo "$track_id $track_name"
                done
            } >$CACHE/albums/$filename
        fi
		set_album_added $CACHE/albums/$filename $added_at
    done
}

request_load ()
{
    test -d $CACHE/albums.bak && rm -r $CACHE/albums.bak
    test -d $CACHE/albums && mv $CACHE/albums $CACHE/albums.bak
    mkdir -p $CACHE/albums $CACHE/albums.bak

    _request_refresh_album
}

album_weight_function ()
{
	added=$(date -u +%s --date=$1)
	bias=$2
	lp_a=$3
	lp_b=$4
	now=$(date -u +%s)
	noise=$(tr -cd '[:digit:]' </dev/urandom |head -c4)
	( bc -l | xargs printf '%05.0f' ) <<EOF
since_added = $now - $added
day = 3600 * 24
month = day * 28

scale=6

r = e (1 - (1.5 * since_added / month))
f = 2 * l (1 + ($lp_b / month))

1000 * (r + f + $bias) + $noise
EOF
}

get_album_weight ()
{
	head -n2 $1 | {
		read data
		read name
		echo $(album_weight_function $data) $name
	}
}

update_album_lp ()
{
	file=$1
	[ -f "$file" ] ||return 1
	now=$(date -u +%s)
	head -n1 $file | {
		read _when _bias lp_a lp_b rest
		d=$((now - lp_a))
		TC=3
		lp_b=$(( (lp_b * (TC - 1) / TC) + (d / TC) ))
		echo $_when $_bias $now $lp_b $rest
		sed '1d' $file
	} >$file.new
	mv $file.new $file
}

select_album1 ()
{
    case $#$1 in
        1-r) PICK="cat" ;;
        0)   PICK="$FZF" ;;
        *-r) PICK="$FZF -f $@" ;;
        *)   PICK="$FZF -q $@" ;;
    esac
	for x in $CACHE/albums/*; do
		get_album_weight $x
	done |sort -r | $PICK | head -n 1

}

select_album2 ()
{
	read weight id rest
    update_album_lp $(echo $CACHE/albums/*_$id | cut -d' ' -f1)
	echo $id $rest
	return
}

select_album ()
{
	select_album1 $@ |select_album2
}

request_queue_album ()
{
    pick=$(select_album "$@")
    [ -z "$pick" ] && {
        echo "no match" > /dev/stderr
        return 1
    }
    echo "$pick"
    if [ -f $CACHE/queue ] && [ "$(tail -n 1 $CACHE/queue)" = "$pick" ] ; then
        echo "skipping duplicate" > /dev/stderr
    else
        echo "$pick" >> $CACHE/queue
    fi
}

request_play ()
{
    pick=$(select_album "$@")
    [ -z "$pick" ] && {
        echo "no match"
        exit 1
    }
    echo "$pick"
    request_resume "spotify:album:$(echo $pick | cut -d ' ' -f 1)"
}

request_queue_get ()
{
    [ -r $CACHE/queue ] && cat $CACHE/queue
    REQUEST GET "me/player/queue" \
        | jq -r ' .queue
| map(@text "[\(.album.name)][\(.track_number)][\(.name)]")
| @sh' | xargs -n 1 echo
}

request_queue_clear ()
{
    rm -f $CACHE/queue
}

quiet_queue_pop ()
{
    sed -i '1d' $CACHE/queue
}

request_queue_pop ()
{
    quiet_queue_pop
    cat $CACHE/queue
}

request_queue_undo ()
{
    sed -i '$d' $CACHE/queue
    cat $CACHE/queue
}

request_queue_rewrite ()
{
	case $1 in
		-f) debug=true ;;
		-d) set -x ;;
		*)  debug=false ;;
	esac
    test -r $CACHE/queue && next=$(head -n1 $CACHE/queue)
    test -z "$next" && next=`select_album -r`
    next=$(echo "$next" | cut -d' ' -f1)

    REQUEST GET "me/player" 2>/dev/null \
        | jq -r '.item | .album.total_tracks, .track_number, .id' \
        | {
        read total_tracks; read track_number; read item_id
        test -n "$track_number" || exit 0
        if ! $debug
        then
            test "$total_tracks" -ne "$track_number" && return 0
            grep -Fx "$item_id" $CACHE/rewrite >/dev/null 2>&1 && return 0
        fi
		next=$(ls -l $CACHE/albums/*_$next)
		if [ $(echo "$next" |wc -l) -ne 1 ]; then
			echo "error: selected track not found" >&2
			return 1
		fi
        tail -n +3 $next | while read track rest
        do
			echo "add track $track $rest" >/dev/stderr
            REQUEST POST "me/player/queue?uri=spotify:track:$track"
			sleep 0.1
        done
        echo "$item_id" >$CACHE/rewrite
        printf "current %2d/%2d %s adding %s" \
               $track_number $total_tracks $item_id $next >>~/log/rewrite
        request_queue_pop
    }
}

if [ "$#" -eq "0" ] ; then
    request_status
    exit 0
fi

if [ "$1" = -r ] ; then
    [ -z "$2" ] && exit 1
    FMT="@text \"$2\""
    REQUEST GET me/player | jq -j "$FMT"
    request_queue_rewrite >>$HOME/log/rewrite
    exit 0
fi

if [ "$1" = -q ]; then
    REQUEST GET $2
    exit 0
fi

requests="\
back
device
forward
load
play
queue album
queue clear
queue get
queue pop
queue rewrite
queue undo
resume
stop
"

args=$1
while [ "$#" -gt "0" ] ; do
    pattern="$(echo "$args" | sed 's/ /\\w* /g')\w*"
    match="$(echo "$requests" | grep "^$pattern")"
    shift; args="$args $1"
    if [ "$(echo "$match" | wc -l)" -eq "1" ] && ! [ -z "$match" ] ; then
        request_$(echo $match | tr ' ' '_') "$@"
        exit $?
    fi
done

printf "==> input matched:\n${match}\n==> available:\n$requests\n" 2>&1

exit 1
