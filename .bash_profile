export BASH_DEPTH=0
[ $TERM = linux ] && setterm -blength 0

# pick a color to show the host name in from colors 1-6 and 9-14
name_hash=0x$(shasum /etc/hostname | head -c 8)
host_color_idx=$(( ( $name_hash / 2 % 6  + 1 + 8 * $name_hash % 2 ) + 30 ))
export host_color_idx

export BASH_ENV=~/.env
test -x $BASH_ENV && . $BASH_ENV
test -x ~/.bashrc && . ~/.bashrc
