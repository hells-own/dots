hi clear
let g:colors_name = 'l'
set background=dark

hi String ctermfg=1
hi link Character String

" Type
hi Type ctermfg=2

hi Comment ctermfg=3

hi Number ctermfg=4
hi link Float Number

hi Statement term=NONE ctermfg=5
" Conditional Repeat Label Operator Keyword Exception
hi link Boolean Statement
hi link PreProc Statement

hi Identifier term=NONE ctermfg=6

" cursor
" Cursor		Character under the cursor.
" lCursor		Character under the cursor when |language-mapping|
" CursorIM	Like Cursor, but used when in IME mode. |CursorIM|

" em
" Directory	Directory names (and other special names in listings).
" DiffText	Diff mode: Changed text within a changed line. |diff.txt|
" VertSplit	Column separating vertically split windows.
" IncSearch	'incsearch' highlighting; also used for the text replaced with
" CursorLineNr	Like LineNr when 'cursorline' is set and 'cursorlineopt'

" MatchParen	Character under the cursor or just before it, if it
" PmenuSel	Popup menu: Selected item.
" PmenuKindSel	Popup menu: Selected item "kind".
" PmenuExtraSel	Popup menu: Selected item "extra text".
" Search		Last search pattern highlighting (see 'hlsearch').
" CurSearch	Current match for the last search pattern (see 'hlsearch').
" SpecialKey	Meta and special keys listed with ":map", also for text used
" WildMenu	Current match in 'wildmenu' completion.
" Changed

" pos
" DiffAdd		Diff mode: Added line. |diff.txt|
" Added

" neg
" DiffDelete	Diff mode: Deleted line. |diff.txt|
" ErrorMsg	Error messages on the command line.
" Error
" Removed
hi ErrorMsg cterm=bold ctermfg=Red ctermbg=Black
hi link Error ErrorMsg

" Todo
" WarningMsg	Warning messages.

" hint
" ColorColumn	Used for the columns set with 'colorcolumn'.
" Conceal		Placeholder characters substituted for concealed
" CursorColumn	Screen column that the cursor is in when 'cursorcolumn' is set.
" CursorLine	Screen line that the cursor is in when 'cursorline' is set.
" DiffChange	Diff mode: Changed line. |diff.txt|
" EndOfBuffer	Filler lines (~) after the last line in the buffer.
" Folded		Line used for closed folds.
" FoldColumn	'foldcolumn'
" SignColumn	Column where |signs| are displayed.
" CursorLineFold	Like FoldColumn when 'cursorline' is set for the cursor line.
" CursorLineSign	Like SignColumn when 'cursorline' is set for the cursor line.
" NonText		'@' at the end of the window, "<<<" at the start of the window

" vis
" Visual		Visual mode selection.

" normal
" LineNr		Line number for ":number" and ":#" commands, and when 'number'
" LineNrAbove	Line number for above the cursor line.
" LineNrBelow	Line number for below the cursor line.
" ModeMsg		'showmode' message (e.g., "-- INSERT --").
" MoreMsg		|more-prompt|
" Normal		Normal text.
" Pmenu		Popup menu: Normal item.
" PmenuKind	Popup menu: Normal item "kind".
" PmenuExtra	Popup menu: Normal item "extra text".
" PmenuSbar	Popup menu: Scrollbar.
" PmenuThumb	Popup menu: Thumb of the scrollbar.
" PopupNotification
" Question	|hit-enter| prompt and yes/no questions.
" QuickFixLine	Current |quickfix| item in the quickfix window.
" SpellBad	Word that is not recognized by the spellchecker. |spell|
" SpellCap	Word that should start with a capital. |spell|
" SpellLocal	Word that is recognized by the spellchecker as one that is
" SpellRare	Word that is recognized by the spellchecker as one that is
" StatusLine	Status line of current window.
" StatusLineNC	status lines of not-current windows
" StatusLineTerm	Status line of current window, if it is a |terminal| window.
" StatusLineTermNC	Status lines of not-current windows that is a
" TabLine		Tab pages line, not active tab page label.
" TabLineFill	Tab pages line, where there are no labels.
" TabLineSel	Tab pages line, active tab page label.
" Terminal	|terminal| window (see |terminal-size-color|).
" Title		Titles for output from ":set all", ":autocmd" etc.
" VisualNOS	Visual mode selection when vim is "Not Owning the Selection".
