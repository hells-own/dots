# detect chroot
test $USER = root \
    && test "$(stat -c %d:%i /)" != "$(stat -c %d:%i /proc/1/root/.)" \
    && export CHROOT=yes \
        || :

bash_term="[\033[36m$(tty | xargs realpath --relative-to /dev)\033[0m]"

precmd ()
{
    ps1_status="$?"
    ps1_jobs="$(jobs | wc -l)"
    if test $ps1_status = 0 ; then
        ps1_status=
    else
        ps1_status="[\033[1m${ps1_status}\033[0m]"
    fi
    if test $ps1_jobs = 0 ; then
        ps1_jobs=
    else
        ps1_jobs="[\033[4m${ps1_jobs}\033[0m]"
    fi
    test -n "$DISPLAY" &&  printf "\e]0;%s\a" "bash $USER@$HOSTNAME $PWD"
}

PROMPT_COMMAND=(precmd)

export bash_term

# set prompt
PS1='${bash_term}${ps1_jobs}${ps1_status}[\033[32m\w\033[0m]
\033[1;${host_color_idx}m\u@\h\033[0;37m\$ '

# show chroot in prompt
test -n "$CHROOT" && PS1="[%F{red}chroot%f]$PROMPT" || :
