. $HOME/.env
export ZSH_DEPTH=0

[ "$TERM" = linux ] && setterm -blength 0

# pick a color to show the host name in from colors 1-6 and 9-14
str_to_clr ()
{
    shasum | head -c8 | {
        read hash
        printf $(( (0x$hash % 6) + 1 ))
    }
}

id_host_user ()
{
    for x in $(seq 10)
    do
        whoami
        cat /etc/hostname
    done
}

export LZSH_HC=$(str_to_clr </etc/hostname)
export LZSH_UC=$(id_host_user | str_to_clr)

# set prompt
export OPROMPT="\
%(1j.%F{blue}[%j]%f.)\
[%F{cyan}%y%f]\
[%F{green}%~%f]\
%(?..(%F{red}%?%f%))
%B%F{$LZSH_UC}%n%f@%F{$LZSH_HC}%m%f%#%b "

# detect chroot
test $USER = root \
    && test "$(stat -c %d:%i /)" != "$(stat -c %d:%i /proc/1/root/.)" \
    && export CHROOT=yes \
    || :

# show chroot in prompt
test -n "$CHROOT" && 0PROMPT="[%F{red}chroot%f]$0PROMPT" || :

if test -d $HOME/.env.d; then
    for env in $HOME/.env.d/*; do
        . $env
    done
fi

export PROMPT="$OPROMPT"
