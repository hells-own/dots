;; TODO
;; SubWord movement evil regex

(normal-top-level-add-to-load-path '("/usr/share/gtags"))
(normal-top-level-add-to-load-path '("/usr/share/clang"))

;; (require 'package)
;; (require 'use-package)
;; (unless package--initialized
;;   (package-initialize))
;; (unless package-archive-contents
;;   (package-refresh-contents))

;; (defun pkg-search (&rest search-props)

;; (package-install (package-desc-create :name "gnu-elpa-keyring-update"))

(let ((default-directory "~/.emacs.d/pkg/"))
  (normal-top-level-add-to-load-path
   '("evil" "goto-chg" "compat"
     "corfu" "corfu-terminal" "popon"
     "orderless" "vertico"
     "ggtags"
	 "evil-snipe"
	 "emacs-clang-format-plus"
     )))

(require 'compile)
(require 'display-line-numbers)
(require 'whitespace)

(require 'evil)
(require 'evil-snipe)
(evil-snipe-mode 1)
(require 'orderless)
(require 'vertico)

(require 'clang-format+)

;; (autoload 'rust-mode "rust-mode" nil t)

(setq-default corfu-terminal-mode t)

(load "~/.emacs.d/functions.el")
(let ((local-cfg "~/.emacs.d/local.el"))
  (when (file-exists-p local-cfg)
	(load local-cfg)))

(global-hl-line-mode)
(global-display-line-numbers-mode)
(global-whitespace-mode)
(menu-bar-mode -1)

(evil-mode 1)
(vertico-mode)

(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

;; hooks

(setq
 system-name-cache
 (string-trim (shell-command-to-string "cut -d' ' -f1 /etc/hostname"))
 user-name-cache
 (string-trim (shell-command-to-string "whoami")))

(defconst my-cc-style
  '("cc-mode"
	(c-offsets-alist . ((innamespace . [0])))))

(c-add-style "my-cc-style" my-cc-style)

(defun makefile-set-tabs ()
  (setq indent-tabs-mode t))

(add-hook 'buffer-list-update-hook #'xterm-print-buffer-file-name)
(add-hook 'find-file-hook          #'init-buffer-project)
(add-hook 'makefile-mode-hook      #'makefile-set-tabs)
(add-hook 'org-mode-hook           #'turn-on-font-lock)

(defun init-corfu ()
  (interactive)
  (require 'corfu)
  (require 'corfu-terminal)
  (corfu-mode +1)
  (corfu-terminal-mode +1))

(defun init-corfu-ggtags ()
  (interactive)
  (init-corfu)
  (require 'ggtags)
  (ggtags-mode +1))

(defun set-c-tab-always-indent ()
  (interactive)
  (setq
   c-tab-always-indent nil
   c-basic-offset 4
   tab-width 4))

(add-hook 'emacs-lisp-mode-hook    #'init-corfu)
(add-hook 'c-mode-hook             #'init-corfu-ggtags)
(add-hook 'c++-mode-hook           #'init-corfu-ggtags)
(add-hook 'python-mode-hook        #'init-corfu-ggtags)

(add-hook 'c-mode-common-hook      #'set-c-tab-always-indent)
;; (add-hook 'c-mode-common-hook 	   #'init-clang-format)

;; (defun our-clang-format-buffer ()
;;   (interactive)
;;   (evil-ex-call-command nil "%!clang-format" nil))

;; (defun init-clang-format ()
;;   (interactive)
;;   (require 'clang-format)
;;   (evil-define-key 'normal 'local
;; 	(pfx0 "c") 'clang-format-buffer
;; 	))

(setq
 display-buffer-overriding-action
 '((display-buffer-reuse-window) (reusable-frames . 0)))

;; config

(dolist (pair
	 '(
	   ("\\.cs\\'"		. c-mode)
	   ("\\.shader\\'" 	. c-mode)
	   ("\\.compute\\'"	. c-mode)
           ("\\.uss\\'"		. css-mode)
	   ("\\.tpp\\'"		. c++-mode)
           ("\\.log\\'"		. view-mode)
           ("\\.uxml\\'"	. nxml-mode)
	   ("\\.rs\\'"		. rust-mode)
	   ))
  (add-to-list 'auto-mode-alist pair))

(add-to-list
 'compilation-error-regexp-alist
 '("^\\([a-zA-Z0-9/\.]+\\)(\\([0-9]+\\),\\([0-9]+\\)):" 1 2 3))

(setq
 auto-save-default nil
 c-default-style "linux"
 column-number-mode t
 compilation-scroll-output t
 completion-category-defaults nil
 completion-category-overrides '((file (styles . (partial-completion))))
 completion-styles '(orderless basic)
 display-line-numbers-grow-only t
 display-line-numbers-major-tick 10
 display-line-numbers-type 'relative
 display-line-numbers-width 4
 evil-regex-search t
 evil-search-wrap nil
 evil-want-minibuffer t
 frame-background-mode 'dark
 gc-cons-threshold 5000000
 gud-keep-buffer 't
 make-backup-files nil
 minibuffer-message-timeout nil
 orderless-matching-styles '(orderless-literal orderless-regexp orderless-flex)
 sort-fold-case nil
 truncate-lines nil
 vc-follow-symlinks nil
 vertico-count 50
 visible-cursor nil
 whitespace-style '(face trailing
			 lines-tail missing-newline-at-eof empty)
 whitespace-line-column 90
 org-todo-keywords '(('sequence "WAIT" "TODO" "DONE"))
 org-todo-keyword-faces '(("WAIT" . "cyan")
                          ("TODO" . "yellow")
                          ("DONE" . "magenta"))
 )

(setq-default
 indent-tabs-mode t
 tab-always-indent nil
 c-tab-always-indent nil
   c-basic-offset 4
 tab-width 4
 mode-line-format
 '("%e"
   "%b %*%F "
   user-name-cache "@" system-name-cache " " project-dir-name " "
   "%l:%c "
   vc-mode " "
   compilation-mode-line-errors " "
   )
 )

(setq
   c-basic-offset 4
  tab-width 4)

;; faces

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-builtin-face ((t (:inherit nil :foreground "magenta"))))
 '(font-lock-comment-face ((t (:inherit nil :foreground "yellow"))))
 '(font-lock-constant-face ((t (:inherit nil :foreground "blue"))))
 '(font-lock-function-name-face ((t (:inherit font-lock-variable-name-face))))
 '(font-lock-keyword-face ((t (:inherit font-lock-builtin-face))))
 '(font-lock-string-face ((t (:inherit nil :foreground "red"))))
 '(font-lock-type-face ((t (:inherit nil :foreground "green"))))
 '(font-lock-variable-name-face ((t (:inherit nil :foreground "cyan"))))
 '(highlight ((t (:inherit nil :background "bright black" :bold t))))
 '(hl-line ((t (:inherit nil :background "bright black"))))
 '(line-number-current-line ((t (:inherit nil :foreground "bright red" :bold t))))
 '(line-number-major-tick ((t (:inherit nil :foreground "bright white" :bold t))))
 '(org-level-1 ((t (:inherit nil :foreground "magenta"))))
 '(org-level-2 ((t (:inherit nil :foreground "yellow"))))
 '(org-level-3 ((t (:inherit nil :foreground "cyan"))))
 '(org-level-4 ((t (:inherit nil :foreground "cyan"))))
 '(region ((t (:inherit nil :inverse-video t))))
 '(whitespace-empty ((t (:inherit whitespace-tab))))
 '(whitespace-line ((t (:inherit whitespace-tab))))
 '(whitespace-tab ((t (:inherit nil :background "bright black"))))
 '(whitespace-trailing ((t (:inherit whitespace-tab))))

 '(compilation-error ((t (:inherit nil :foreground "bright red"))))
 '(compilation-warning ((t (:inherit nil :foreground "bright magenta"))))
 '(compilation-infos ((t (:inherit nil :foreground "bright green"))))
 )

(setq
 indicate-state-last (face-foreground 'line-number-current-line)
 indicate-state-default (face-foreground 'line-number-current-line))

(defun indicate-state (bg echo)
  (unless (minibufferp)
    (set-face-foreground 'line-number-current-line bg)
    (unless echo (setq indicate-state-last bg))))

(when (symbolp 'echo-start-hook)
  (add-hook 'echo-start-hook
            #'(lambda () (indicate-state "bright blue" 't)))
  (add-hook 'echo-end-hook
            #'(lambda () (indicate-state indicate-state-last 'f)))
  (add-hook 'evil-emacs-state-entry-hook
            #'(lambda () (indicate-state "bright magenta" nil)))
  (add-hook 'evil-insert-state-entry-hook
            #'(lambda () (indicate-state "bright green" nil)))
  (add-hook 'evil-replace-state-entry-hook
            #'(lambda () (indicate-state "bright cyan" nil)))
  (add-hook 'evil-operator-state-entry-hook
            #'(lambda () (indicate-state "bright yellow" nil)))
  (add-hook 'evil-normal-state-entry-hook
            #'(lambda () (indicate-state indicate-state-default nil))))

(global-prettify-symbols-mode +1)

;; keybinds

(defun pfx0 (key) (kbd (concat "<SPC>" key)))
(defun pfx1 (key) (kbd (concat "<RET>" key)))
(defun pfx2 (key) (kbd (concat "j" key)))

(evil-define-key 'motion 'global
                                        ; dvorak rebinds
  "H" '_evil-scroll-half-page-up
  "h" 'evil-backward-char
  "gj" 'evil-join
  "gJ" 'evil-delete-backward-char-and-join
  "k" 'evil-find-char-to
  "K" 'evil-find-char-to-backward
  "l" 'evil-search-next
  "L" 'evil-search-previous
  "n" 'evil-previous-line
  "N" 'evil-scroll-page-up
  "S" '_evil-scroll-half-page-down
  "s" 'evil-forward-char
  "t" 'evil-next-line
  "T" 'evil-scroll-page-down
  "j" nil
  "J" nil

  "_" 'evil-previous-line-first-non-blank

  ;; intrinsic

  (pfx0 "") nil
  (pfx1 "") nil
  (pfx2 "") nil

  (kbd "<SPC><SPC>") 'evil-write-all
  (kbd "<RET><RET>") 'window-zoom

  (pfx0 "/") 'comment-or-uncomment-region
  (pfx0 "I") 'indent-region-column
  (pfx0 "K") 'save-buffers-kill-emacs
  (pfx0 "V") '(lambda () (open-term 2))
  (pfx0 "W") 'evil-window-vsplit
  (pfx0 "X") 'eval-buffer
  ;; (pfx0 "c") 'our-clang-format-buffer
  (pfx0 "d") 'dired
  (pfx0 "f") 'find-file
  (pfx0 "i") 'indent-region
  (pfx0 "k") 'kill-buffer
  (pfx0 "m") 'smerge-mode
  (pfx0 "n") 'evil-window-next
  (pfx0 "p") 'fill-paragraph
  (pfx0 "q") 'delete-window
  (pfx0 "r") 'reload-config
  (pfx0 "s") 'sort-lines
  (pfx0 "t") 'evil-window-prev
  (pfx0 "u") 'switch-to-buffer
  (pfx0 "v") 'open-term
  (pfx0 "w") 'evil-window-split
  (pfx0 "x") 'eval-expression
  (pfx0 "y") '_urxvt-clip

  "gb" 'project-rebuild-tags
  "gc" 'project-build
  "gC" 'project-rebuild
  "ge" 'next-error
  "gE" 'previous-error
  "gf" 'find-file
  "gF" 'project-find-file
  "gR" 'project-debug
  "gr" 'project-launch
  "gl" 'project-translate

  "ga" 'pop-mark
  "gt" 'xref-find-definitions
  "gT" 'xref-find-definitions-interactively
  )

(evil-define-key* '(normal motion) evil-snipe-local-mode-map
  "s" nil
  "S" nil
  "j" 'evil-snipe-s
  "J" 'evil-snipe-S
  )

(evil-define-key 'insert 'global
  (kbd "<backtab>") #'(lambda () (interactive) (insert-tab)))

(defun call-with-prefix (func)
  (setq current-prefix-arg '(4))
  (call-interactively func))

(defun xref-find-definitions-interactively ()
  (interactive)
  (call-with-prefix 'xref-find-definitions))

(defun ggtags-find-tag-dwim-interactively ()
  (interactive)
  (call-with-prefix 'ggtags-find-tag-dwim))

(evil-define-key 'normal 'global
  "s" nil
  "S" nil)

(evil-define-key 'motion vertico-map
  (kbd "RET") 'vertico-exit
  [remap evil-next-line] 'vertico-next
  [remap evil-previous-line] 'vertico-previous
  [remap evil-scroll-page-down] 'vertico-scroll-up
  [remap evil-scroll-page-up] 'vertico-scroll-down
  )

(defun our-configure-org ()
  (setq org-mode-map (make-keymap))
  (evil-define-key 'motion org-mode-map
    "H" 'org-previous-visible-heading
    "T" 'org-forward-heading-same-level
    "N" 'org-backward-heading-same-level
    "S" 'org-next-visible-heading
    "+" 'org-cycle

    (pfx1 " RET") 'org-meta-return
    (pfx1 "c") 'org-cycle
    (pfx1 "C") 'org-global-cycle
    (pfx1 "h") 'org-metaleft
    (pfx1 "H") 'org-shiftmetaleft
    (pfx1 "n") 'org-metaup
    (pfx1 "N") 'org-shiftmetaup
    (pfx1 "s") 'org-metaright
    (pfx1 "S") 'org-shiftmetaright
    (pfx1 "t") 'org-metadown
    (pfx1 "T") 'org-shiftmetadown
    (pfx1 "m") 'org-todo
    (pfx1 "M") 'org-show-todo-tree
    ))

(eval-after-load 'org #'our-configure-org)

(defun our-configure-ggtags ()
  (setq ggtags-global-window-height 16)
  (evil-define-key 'normal ggtags-mode-map
    "gA" 'ggtags-next-mark
    "ga" 'ggtags-prev-mark
    "gF" 'ggtags-find-file
    "gt" 'ggtags-find-tag-dwim
    "gT" 'ggtags-find-tag-dwim-interactively
    ))
(eval-after-load 'ggtags #'our-configure-ggtags)

(defun our-configure-smerge ()
  (setq smerge-mode-map (make-keymap))
  (evil-define-key 'motion smerge-mode-map
    (pfx2 "l") 'smerge-next
    (pfx2 "L") 'smerge-previous))

(eval-after-load 'smerge #'our-configure-smerge)

(defun our-configure-gud ()
  (setq gud-mode-map (make-keymap))
  (evil-define-key 'normal gud-mode-map
    (pfx2 "b") 'gud-break
    (pfx2 "c") 'gud-cont
    (pfx2 "d") 'gud-down
    (pfx2 "d") 'gud-remove
    (pfx2 "f") 'gud-finish
    (pfx2 "h") 'gud-until
    (pfx2 "i") 'gud-stepi
    (pfx2 "J") 'gud-jump
    (pfx2 "n") 'gud-next
    (pfx2 "p") 'gud-print
    (pfx2 "r") 'gud-refresh
    (pfx2 "R") 'gud-run
    (pfx2 "s") 'gud-step
    (pfx2 "t") 'gud-tbreak
    (pfx2 "u") 'gud-up
    (pfx2 "<RET>") 'gud-prev-expr
    ))
(eval-after-load 'gud #'our-configure-gud)

(defun our-configure-dired ()
  (setq dired-mode-map (make-keymap))
  (evil-define-key nil dired-mode-map
    "+" 'dired-create-directory
    "c" 'dired-do-copy
    "d" 'dired-do-delete
    "g" 'dired-do-chgrp
    "m" 'dired-do-chmod
    "o" 'dired-do-chown
    "r" 'dired-do-rename
    "m" 'dired-mark
    "M" 'dired-unmark
    "q" 'kill-buffer
    "R" 'dired-do-redisplay
    "u" 'dired-undo
    (kbd "RET") 'dired-find-file
    [remap next-line] 'dired-next-line
    [remap previous-line] 'dired-previous-line))

(eval-after-load 'dired #'our-configure-dired)

(defun our-configure-corfu ()
  (define-key corfu-map [remap corfu-complete] #'corfu-next)
  (defun our-corfu-quit ()
    (interactive)
    (evil-normal-state)
    (corfu-quit))
  (evil-define-key 'insert 'corfu-map
    (kbd "<escape>") 'our-corfu-quit)
  (setq
   corfu-cycle t
   corfu-preview-current nil
   corfu-quit-at-boundary nil
   corfu-quit-no-match t
   ))

(eval-after-load 'corfu #'our-configure-corfu)

(defun our-configure-info ()
  (setq Info-mode-map (make-keymap))
  (evil-define-key nil Info-mode-map
    (kbd "<escape>") nil
    "g/" 'Info-search
    "g?" 'Info-search-backward
    "C" 'Info-index
    "c" 'Info-toc
    "I" 'Info-search-next
    "i" 'Info-follow-nearest-node
    "O" 'Info-backward-node
    "o" 'Info-forward-node
    "r" 'Info-next-reference
    "R" 'Info-prev-reference
    "u" 'Info-history-back
    "U" 'Info-history-forward
    "gU" 'Info-top-node
    "gu" 'Info-up
    "x" 'Info-next
    "X" 'Info-prev
    ))
(eval-after-load 'info #'our-configure-info)

(evil-define-key nil view-mode-map
  kbd "e" 'evil-edit)

(defun nop ()
  (interactive))

(evil-define-key 'normal 'global
    "ga" 'pop-tag-mark)

(defun undef-esc (&rest keymaps)
  (dolist (km keymaps)
    (evil-define-key nil km
      (kbd "ESC") #'nop)))

(undef-esc 'global help-mode-map)
(undef-esc 'global compilation-mode-map)
