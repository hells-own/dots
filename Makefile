all:

MAKE += -k

LN := ln -sT
ifdef F
LN += -f
endif

ifndef PREFIX
PREFIX := $(HOME)
endif

PWD         := $(realpath .)
DB_PATH     := db
IGNORE_PATH := ignore

$(shell touch $(DB_PATH))

INSTALLED := $(file <$(DB_PATH))
IGNORE    := $(file <$(IGNORE_PATH)) $(IGNORE_PATH)
AVAILABLE := $(shell git ls-files)
WANT      := $(addprefix $(PREFIX)/,$(filter-out $(IGNORE),$(AVAILABLE)))
ADD       := $(filter-out $(INSTALLED),$(WANT))
RM        := $(filter-out $(WANT),$(INSTALLED))

ifdef DRY
X = @echo
endif

less_ver := $(word 2,$(shell less -V))
ifneq (,$(intcmp $(less_ver),582,lt))
less_snt := .lesskey_sentinel
$(less_snt) : .lesskey
	lesskey
	@touch $@
all: $(less_snt)
endif

install : $(ADD)
ifneq ($(RM),)
	$Xrm -f $(RM)
endif
ifneq ($(WANT),$(INSTALLED))
	@echo $(WANT) >$(DB_PATH)
endif

$(ADD) : $(PREFIX)/% : %
	$X@mkdir -p $(dir $(@))
	$X$(LN) $(PWD)/$(<) $(@)

clrscm = $(HOME)/.config/.colorscheme_snt

$(clrscm): bin/rebuild-colorschemes bin/gen-colors
	$X./$<
	$Xtouch $@

all      : install hooks $(clrscm)

clean:
	$Xrm -f $(DB_PATH) $(INSTALLED) $(clrscm)

.PHONY   : all install clean hooks $(ADD)
