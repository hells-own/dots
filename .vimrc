map <Enter><Enter> :only<Enter>
map <Space><Space> :w<Enter>
map <Space>R :source $MYVIMRC <Enter>
map <Space>h :winc h<Enter>
map <Space>k :tabc<Enter>
map <Space>n :winc k<Enter>
map <Space>s :winc l<Enter>
map <Space>t :winc j<Enter>

map <Space>f :tabedit

noremap H <C-U>
noremap K T
noremap L N
noremap N <C-B>
noremap S <C-D>
noremap T <C-F>
noremap k t
noremap l n
noremap n k
noremap s l
noremap t j

filetype plugin indent on
syntax on

set hidden
set hlsearch
set incsearch
set ignorecase
" this is the STATUS line, not the command line...
set laststatus=3
set listchars=tab:>-,trail:.,extends:>,precedes:<
set matchpairs+=<:>
set nowrapscan
set number
set relativenumber
set shiftround
set shiftwidth=8
set smartcase
set softtabstop=8
set tabstop=8
set wildmenu
set wildmode=longest,list,full
set wrap

command -bar -nargs=? -complete=help Thelp execute TabHelp (<q-args>)
function TabHelp (subject) abort
	let mods = 'silent noautocmd keepalt'
	let matched = getcompletion (a:subject, 'help') 
	if ! empty (matched)
		echo 'matched ' .. matched[0]
		execute mods .. ' tabedit ' .. &helpfile
		set buftype=help
	endif
	return 'help ' .. a:subject
endfunction

command -nargs=0 Tlist execute TabList ()

function TabList ()
	for tab in gettabinfo ()
		let tabnr = tab['tabnr']
		echo 'tab ' .. tabnr
		for wnr in tab['windows']
			echo '  win ' .. wnr
			let winvar = gettabwinvar (tabnr, wnr, '')
			for key in keys (winvar)
				echo '    ' .. key .. '=' .. winvar[key]
			endfor
		endfor
	endfor
endfunction

command -nargs=1 ProjectShell execute 'terminal ' ..  ProjectShell (<args>)
function ProjectShell (cmd)
	return 'project-shell ' .. expand ("%:p:h") .. ' ' .. a:cmd .. ' ' .. expand ("%:p")
endfunction

source ~/.vim/colors/l.vim
